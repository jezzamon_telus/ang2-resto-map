// Angular
import '@angular/platform-browser';
import '@angular/platform-browser-dynamic';
import '@angular/core';
import '@angular/common';
import '@angular/http';
import '@angular/router';
// RxJS
import 'rxjs';
// Other vendors for example jQuery, Lodash or Bootstrap
// You can import js, ts, css, sass, ...
import 'script!jquery';
// import 'foundation-sites/dist/js/foundation.js';
import '../public/sass/styles.scss';

import '../public/js/jquery.min.js';
import '../public/js/bootstrap.min.js';
import '../public/js/material.min.js';
import '../public/js/material-kit.js';
import '../public/js/custom.js';

// import '../public/js/jasny-bootstrap.min.js';
import '../public/js/jquery.dropdown.js';


