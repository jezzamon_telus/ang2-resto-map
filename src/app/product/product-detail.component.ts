import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Resto } from '../service/resto';

@Component({
  templateUrl: './product-detail.component.html'
})
export class ProductDetailComponent implements OnInit {
  pageTitle: string = 'Product Detail';
  product: Resto;

  constructor(private _route: ActivatedRoute, private _router: Router) {}

  ngOnInit(): void {
    let id = +this._route.snapshot.params['id']; // + is a js shortcut here to convert params id from string to integer
    this.pageTitle += `: ${id}`;
  }

  onBack(): void {
    this._router.navigate(['/products']);
  }
}

