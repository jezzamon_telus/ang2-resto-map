import { Component, OnInit } from '@angular/core';

import { RestoService } from './../service/restos.service'
import { Resto } from '../service/resto';

@Component({
  templateUrl: './products.component.html'
})
export class ProductsComponent implements OnInit {
  pageTitle: string = 'Product Detail';
  mocks: Resto[];
  errorMessage: any;

  constructor(private restoSVC: RestoService ) {
  }

  ngOnInit():void {
    console.log('onInit');

    this.restoSVC.getMock()
      .subscribe(products => this.mocks = products,
                 error => this.errorMessage = <any>error);
  }

}