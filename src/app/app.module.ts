import { NgModule } from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { WindowRef } from './shared/windowRef.service';
import { AppComponent } from './start/app.component';

import { NavComponent } from './shared/navbar.component';
import { HomeComponent } from './home/home.component';
import { ErrorComponent } from './error/error.component';
import { AppRoutingModule } from './shared/app.routing';
import { StarComponent } from './shared/star.component';
import { ProductsComponent } from './product/products.component';
import { ProductDetailGuard } from './product/product-guard.service';
import { ProductDetailComponent } from './product/product-detail.component';


import { TruncatePipe } from './shared/trunc.pipe';
import { FilterRestosPipe } from './home/filterRestos.pipe';




@NgModule({
    imports: [
        BrowserModule,
        CommonModule,
        FormsModule,
        AppRoutingModule,
        HttpModule
    ],
    exports: [ TruncatePipe ],
    declarations: [
        AppComponent,
        NavComponent,
        HomeComponent,
        ErrorComponent,
        TruncatePipe,
        FilterRestosPipe,
        StarComponent,
        ProductsComponent,
        ProductDetailComponent
    ],
    providers: [
      WindowRef,
      ProductDetailGuard
    ],
    bootstrap: [ AppComponent ]
})
export class AppModule { }
