export interface IPlaces {
  name: string;
  vicinity: string;
  rating: string;
  types: Array<string>,
  photos: Array<any>,
  geometry: any;
}