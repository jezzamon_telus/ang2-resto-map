import { Component, OnInit} from '@angular/core';

import { WindowRef } from '../shared/windowRef.service';
import { IPlaces } from './places';
import { Resto } from './../service/resto';


@Component({
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  mapDiv: HTMLElement;
  mapData: any;
  window: any;
  places: IPlaces[];
  map: any;
  location: string;
  currentLat: number;
  currentLng: number;
  nearby: any;
  markers: Array<any>;
  searchInput: HTMLElement;
  inputLat: number = 0;
  inputLng: number = 0;
  listFilter: string = null;
  ratingTitle: string;
  errorMessage: any;
  mocks: Resto[];


  constructor(private winRef: WindowRef) {

    this.window = winRef.nativeWindow;
    this.getNearMeCb = this.getNearMeCb.bind(this);
    this.makeMap = this.makeMap.bind(this);
    this.getCurrentLocation = this.getCurrentLocation.bind(this);
    this.autocomplete = this.autocomplete.bind(this);
  }

  ngOnInit():void {
    console.log('onInit');
    
    this.getCurrentLocation()
      .then(this.makeMap)
      .then(data => console.log('promise returned info',data))
      .catch(err=> {
        console.log("err!", err)
      });
    
    this.autocomplete();
  }

  onRatingClicked(message: string): void {
    this.ratingTitle = ' Resto : ' + message;
  }

  autocomplete() {
    
    this.searchInput = document.getElementById('address');

    const latDiv =  document.getElementById('lat');
    const lngDiv =  document.getElementById('lng');
    

    const dropdown = new this.window.google.maps.places.Autocomplete(this.searchInput);

    

    dropdown.addListener('place_changed', () => {
      const place = dropdown.getPlace();  //console log this to see all the info google provides
      this.inputLat = place.geometry.location.lat(); //can grab html input attr with dot notation
      this.inputLng = place.geometry.location.lng();
      console.log(this.inputLat)
      console.log(this.inputLng)
    })
  }

  search() {
    console.log('click!')
    this.currentLat = this.inputLat;
    this.currentLng = this.inputLng;
    console.log(this.currentLat, this.currentLng);

    this.makeMap();
  }

  getCurrentLocation() {
    
    let options = {
      enableHighAccuracy: true,
      timeout: 5000,
      maximumAge: 0
    };
      
    let that = this;

    //wrap geolocation success and error in promise      
    return new Promise(function (resolve, reject) { 
      const success = (pos:any) => {
      
      let crd = pos.coords;

      console.log('Your current position is:');
      console.log(`Latitude : ${crd.latitude}`);
      console.log(`Longitude: ${crd.longitude}`);
      console.log(`More or less ${crd.accuracy} meters.`);

      that.currentLat = crd.latitude;
      that.currentLng = crd.longitude;
      resolve(pos);
      
    };

    const error = (err:any) => {
      console.warn(`ERROR(${err.code}): ${err.message}`);
      reject(err);
    };
      
      navigator.geolocation.getCurrentPosition(success, error, options);
    });

    


  }

  makeMap() {
    // takes mapDiv
    let markers;

    return new Promise( (resolve, reject) => {
      console.log('makemap at', this.currentLat, this.currentLng);

      this.mapDiv = document.getElementById('map');
      this.location = new this.window.google.maps.LatLng(this.currentLat, this.currentLng);

      const mapOptions = {
        center: this.location,
        zoom: 15
      }

      
      this.map = new this.window.google.maps.Map(this.mapDiv, mapOptions);
      

      let nearbyService;

      let request = {
        location: this.location,
        radius: '750',
        type: ['restaurant']
      };

      nearbyService = new this.window.google.maps.places.PlacesService(this.map);
      let info = nearbyService.nearbySearch(request, this.getNearMeCb);

       // create a bounds 
      const bounds = new this.window.google.maps.LatLngBounds();
      // create info window
      
      let infoWindow = new this.window.google.maps.InfoWindow();
      
      
      let that = this;
      let markers:Array<any> = [];
      setTimeout( ()=> {
        
        console.log(this.places);
         for (var i = 0; i < this.places.length; i++) {
          let place = this.places[i]
          const [placeLat, placeLng] = [place.geometry.location.lat(), place.geometry.location.lng()];
          const position = { lat: placeLat, lng: placeLng }

          // add a position the bounds 
         bounds.extend(position);

          // make our google Maps marker (takes two args 1)  instance of map, 2) lat and lng object)
          const marker = new that.window.google.maps.Marker({ map: that.map, position} )

          // add place info to marker
          marker.place = place;
          console.log(marker);
         

          marker.addListener('click', function() {
             const html = `
                <div class"popup" style="width: 300px;">
                  <a href="#">
                    <img style="width: 100%;" src="${place.photos[0].getUrl({ 'maxWidth': 55, 'maxHeight': 55 })}" alt="${place.name}" />
                    <p style="line-height: 1.5; font-size:2.2rem">${place.name} </p>
                  </a>
                </div>
              `;
            // insert html into google infoWindow
            infoWindow.setContent(html);
            // open window on click, two arguments (mapDiv, current marker)
            infoWindow.open(that.map, marker);
          });

           markers.push(marker)
        }
    
        
      },2000);

      this.markers = markers;

      // center  the map using the bounds that remembers all positions 
      // this.map.setCenter(bounds.getCenter()); //TO DEBUG
       
      // set smallest zoom to fit all bounds/markers
      // this.map.fitBounds(bounds); //TO DEBUg
    
    }); //end promise


  }

  getNearMeCb(results:any, status: string) {

    return new Promise( (resolve, reject) => {
       // get places nearby
       
      let tmpPlaces:any = []

      if (status == this.window.google.maps.places.PlacesServiceStatus.OK) {

        for (var i = 0; i < results.length; i++) {
          var place =  results[i];
          // createMarker(results[i]);
          console.log('nearby',place.geometry.location.lat())
          console.log(place.geometry.location.lng())
          tmpPlaces.push(place);
        }
        this.places = tmpPlaces;
        // console.log('this.nearby', this.places)
        
      }
      resolve;
     });

    
    
  }
 
}