import { Pipe, PipeTransform } from '@angular/core';
import { IPlaces } from './places';

@Pipe({name: 'filterResto'})

export class FilterRestosPipe implements PipeTransform {

    transform(places: IPlaces[], filterBy: string): any {
        filterBy = filterBy ? filterBy.toLocaleLowerCase() : null;
        return filterBy ? places.filter(( place: IPlaces) => 
            place.name.toLocaleLowerCase().indexOf(filterBy) !== -1) : places;
    }
    
}