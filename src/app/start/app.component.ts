import { Component } from '@angular/core';

 import { RestoService } from './../service/restos.service';


@Component({
  selector: 'my-app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [RestoService]
})
export class AppComponent { }
