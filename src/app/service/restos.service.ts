import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable'; 
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';

import { Resto } from './resto'


@Injectable()
export class RestoService {
  private _productUrl = 'api/products/products.json';
  constructor(private httpSVC: Http ) {

  }
  getMock(): Observable<Resto[]> {
    return this.httpSVC.get(this._productUrl)
      .map((response: Response) => <Resto[]> response.json())
      .do(data => console.log('All: ' + JSON.stringify(data)))
      .catch(this.handleError);
  }

  private handleError(error: Response) {
    console.error(error);
    return Observable.throw(error.json().error || 'server error');
  }
}