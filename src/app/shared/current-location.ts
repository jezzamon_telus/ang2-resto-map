export class CurrentLocation {
  constructor(
    public lat: string,
    public lng: string
  ){}
}