export class NearMe {
  constructor(
    public name: string,
    public geometry: object,
    public vicinity: string
  ){}
}