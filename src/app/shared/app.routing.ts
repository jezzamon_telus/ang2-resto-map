import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { HomeComponent } from '../home/home.component';
import { ErrorComponent } from '../error/error.component';
import { ProductsComponent } from '../product/products.component';
import { ProductDetailGuard } from '../product/product-guard.service';
import { ProductDetailComponent } from '../product/product-detail.component';

@NgModule({
    imports: [
        RouterModule.forRoot([
            { path: 'resto' , component: HomeComponent},
            { path: 'products' , component: ProductsComponent},
            { path: 'products/:id' ,
              canActivate: [ProductDetailGuard], component: ProductDetailComponent},
            { path: '' , redirectTo: 'resto', pathMatch: 'full'},
            { path: '**' , component: ErrorComponent }
        ])    
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule {}

